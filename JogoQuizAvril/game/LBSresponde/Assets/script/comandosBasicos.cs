﻿using UnityEngine;
using System.Collections;
using LocalizationEditor;

public class comandosBasicos : MonoBehaviour {

    private AudioSource m_Audio;
    private ChartbostController m_chartboost;

    void Start()
    {
        m_Audio = gameObject.GetComponent<AudioSource>();
        if (AudioController.Instance.IsEnable())
            m_Audio.Play();

        m_chartboost = GameObject.FindObjectOfType<ChartbostController>();
        m_chartboost.CacheInterstitial();
    }

	public void carregaCena()
    {
		Application.LoadLevel(Constants.LoadingScene);
    }

    public void SetEnglishLanguage()
    {
        PlayerPrefs.SetString("Localize", Constants.EN);
        LEManager.CurrentLocSet = Constants.EN;
    }
    public void SetPortugueseLanguage()
    {
        PlayerPrefs.SetString("Localize", Constants.PT);
        LEManager.CurrentLocSet = Constants.PT;
    }
}
