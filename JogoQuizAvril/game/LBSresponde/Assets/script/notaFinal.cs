﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Advertisements;

public class notaFinal : MonoBehaviour {

    private int idTema;
    public Text txtNota;
    public GameObject SoundOn, SoundOff;

    private int notaF;
    private int acertos;
    private AudioSource m_Audio;

    void Awake()
    {
        m_Audio = GetComponent<AudioSource>();
        if (AudioController.Instance.IsEnable())
            m_Audio.Play();
    }

	void Start () {

        idTema = PlayerPrefs.GetInt("idTema");
        acertos = PlayerPrefs.GetInt("acertos");
        txtNota.text = acertos.ToString();

        if (AudioController.Instance.IsEnable())
            SetButtonSound(SoundOn, SoundOff);
        else
            SetButtonSound(SoundOff, SoundOn);
	}

    public void jogarNovamente()
    {
		Application.LoadLevel(Constants.LoadingScene);
    }

    public void GotoHome()
    {
        Application.LoadLevel(Constants.TituloScene);
    }

    public void ShowLeaderboard()
    {
        ServicesManager.Instance.ShowLeaderboard(Constants.LeaderboardId);
    }

    public void MuteSounds()
    {
        AudioController.Instance.MuteAllSounds();
        SetButtonSound(SoundOff, SoundOn);
    }

    public void UnMuteSounds()
    {
        AudioController.Instance.EnableAllSound();
        SetButtonSound(SoundOn, SoundOff);
    }

    private void SetButtonSound(GameObject soundOn, GameObject soundOff)
    {
        soundOn.SetActive(true);
        soundOff.SetActive(false);
    }
}
