﻿using UnityEngine;
using System.Collections;
using LocalizationEditor;

public class IntroManager : MonoBehaviour {

	void Awake () {

        if (!PlayerPrefs.HasKey("Localize"))
        {
            PlayerPrefs.SetString("Localize", Constants.PT);
            LEManager.CurrentLocSet = Constants.PT;
        }
        else
        {
            string lang = PlayerPrefs.GetString("Localize");
            if (lang.Equals(Constants.PT))
                LEManager.CurrentLocSet = Constants.PT;
            else if (lang.Equals(Constants.EN))
                LEManager.CurrentLocSet = Constants.EN;
            else
                Debug.Log("N foi possivel definir linguagem");
        }

        Invoke("ChangeScene", 2.5f);
	}

    void ChangeScene()
    {
        Application.LoadLevel(Constants.TituloScene);
    }
}
