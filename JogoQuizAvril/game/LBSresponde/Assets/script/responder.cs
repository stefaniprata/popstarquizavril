﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine.Advertisements;
using System;
using LocalizationEditor;

public class responder : MonoBehaviour {

    private int idTema;

    public Text pergunta;
    public Text respostaA;
    public Text respostaB;
    public Text respostaC;
    public Text respostaD;
    public Image GreenBar;
    public Sprite img_Right, img_Wrong, img_Normal;
    public AudioClip sng_Right, sng_Wrong;
    public AudioSource BgSound;
    public GameObject ModalContinue;

    public Button[] Buttons;
    public List<string> perguntas; //armazena todas as perguntas
    public List<string> alternativaA; //armazena todas as alternativas...
    public List<string> alternativaB;
    public List<string> alternativaC;
    public List<string> alternativaD;
    public List<string> corretas; // armazena as alternativas corretasa

    private int idPergunta;
    private int acertos;
    private float questoes;
    private float media;
    private int notaFinal;
    private float timeQuestion = 10.0f;
    private bool questionAnswered, questionsIsOver, justOnce, isConnected;
    private int randomIndex;
    private AudioSource audioSource;
    private ChartbostController m_chartboost;

    private List<string> perguntaRespondida;
    private List<string> alternativaARespondida;
    private List<string> alternativaBRespondida;
    private List<string> alternativaCRespondida;
    private List<string> alternativaDRespondida;
    private List<string> corretaRespondida;

    void Awake()
    {
        if (AudioController.Instance.IsEnable())
            BgSound.Play();

        m_chartboost = GameObject.FindObjectOfType<ChartbostController>();
        m_chartboost.CacheInterstitial();

        InitializeLists();
    }

    void Start () {
        idTema = PlayerPrefs.GetInt("idTema");
        idPergunta = 0;
        questoes = perguntas.Count;
        audioSource = this.GetComponent<AudioSource>();

        InitializeQuestionsAnswerded();

        RequestQuestion();

        //Check Internet connection
        StartCoroutine(checkInternetConnection((bool success) =>
        {
            if (success) isConnected = true;
            else isConnected = false;
        }));
	}

    void Update()
    {
        if (GreenBar == null) Debug.Log("yellow ta nulo cara!!!");
        timeQuestion -= Time.deltaTime;
        if(timeQuestion <= 0)
        {
            GreenBar.fillAmount = 1;
            timeQuestion = questionsIsOver == false ? 10.0f : 6.0f;
            questionAnswered = false;
            Application.LoadLevel("notaFinal");
        }
        if (timeQuestion > 0 && questionAnswered)
        {
            timeQuestion = questionsIsOver == false ? 10.0f : 6.0f;
            GreenBar.fillAmount = 1;
            questionAnswered = false;
        }

        TimeBarFilled();
    }

    public void resposta(string alternativa)
    {
        if(alternativa == "A")
            VerifyAnswer(alternativaA[randomIndex], Buttons[0]);

        else if (alternativa == "B")
            VerifyAnswer(alternativaB[randomIndex], Buttons[1]);

        else if (alternativa == "C")
            VerifyAnswer(alternativaC[randomIndex], Buttons[2]);

        else if (alternativa == "D")
            VerifyAnswer(alternativaD[randomIndex], Buttons[3]);
    }
    //fim resposta

    private void VerifyAnswer(string answer, Button button)
    {
        questionAnswered = true;
        if (answer == corretas[randomIndex])
        {
            button.spriteState = ChangeSpriteAnswer(img_Right);
            if (AudioController.Instance.IsEnable())
                AudioController.Instance.PlaySoundEffect(audioSource, sng_Right);
            acertos += 1;
            RemoveQuestions(randomIndex);
            RequestQuestion();
        }
        else
        {
            button.spriteState = ChangeSpriteAnswer(img_Wrong);
            if (AudioController.Instance.IsEnable())
                AudioController.Instance.PlaySoundEffect(audioSource, sng_Wrong);

			SaveScore ();
            Invoke("ChangeScene", 0.8f);
        }
    }

    private void SaveScore()
    {
        if (!PlayerPrefs.HasKey("score"))
        {
            PlayerPrefs.SetInt("score", acertos);
            PostScore();
        }

        else
        {
            var value = PlayerPrefs.GetInt("score");
            if (acertos > value)
            {
                PlayerPrefs.SetInt("score", acertos);
                PostScore();
            }
            else
            {
                PostScore();
            }
        }
    }

    void PostScore()
    {
        ServicesManager.Instance.PostScore(acertos, Constants.LeaderboardId, (bool sucess) =>
        {
            if (sucess)
                Debug.Log("Score submetido");
            else
                Debug.Log("Erro ao submeter score");
        });
    }

    void RequestQuestion()
    {
        idPergunta += 1;

        Invoke("ResetButtonsSprites", 0.1f);

        if (perguntas.Count > 0)
        {
            randomIndex = RandomIndexQuestion();
            //atualiza os textos
            pergunta.text = perguntas[randomIndex];
            respostaA.text = alternativaA[randomIndex];
            respostaB.text = alternativaB[randomIndex];
            respostaC.text = alternativaC[randomIndex];
            respostaD.text = alternativaD[randomIndex];
        }
        else
        {
            questionsIsOver = true;
            InitializeQuestion();
            RequestQuestion();
        }
    }

    //Método para contar o tempo pra cada pergunta
    private void TimeBarFilled()
    {
        if(!questionsIsOver)
            GreenBar.fillAmount -= 1.0f / 10 * Time.deltaTime;
        else
            GreenBar.fillAmount -= 1.0f / 7 * Time.deltaTime;
    }

    private SpriteState ChangeSpriteAnswer(Sprite sprite)
    {
        SpriteState sprState = new SpriteState();
        sprState.pressedSprite = sprite;
        sprState.highlightedSprite = sprite;
        sprState.disabledSprite = sprite;

        return sprState;
    }

    private void ResetButtonsSprites()
    {
        foreach (var item in Buttons)
            item.spriteState = ChangeSpriteAnswer(img_Normal);
    }

    private int RandomIndexQuestion()
    {
        var numberElements = perguntas.Count;
        return UnityEngine.Random.Range(0, numberElements - 1);
    }

    private void RemoveQuestions(int index)
    {
        perguntas.RemoveAt(index);
        alternativaA.RemoveAt(index);
        alternativaB.RemoveAt(index);
        alternativaC.RemoveAt(index);
        alternativaD.RemoveAt(index);
        corretas.RemoveAt(index);
    }

    private void InitializeQuestionsAnswerded()
    {
        perguntaRespondida = new List<string>();
        alternativaARespondida = new List<string>();
        alternativaBRespondida = new List<string>();
        alternativaCRespondida = new List<string>();
        alternativaDRespondida = new List<string>();
        corretaRespondida = new List<string>();

        perguntaRespondida.AddRange(perguntas);
        alternativaARespondida.AddRange(alternativaA);
        alternativaBRespondida.AddRange(alternativaB);
        alternativaCRespondida.AddRange(alternativaC);
        alternativaDRespondida.AddRange(alternativaD);
        corretaRespondida.AddRange(corretas);
    }

    private void InitializeQuestion()
    {
        perguntas.AddRange(perguntaRespondida);
        alternativaA.AddRange(alternativaARespondida);
        alternativaB.AddRange(alternativaBRespondida);
        alternativaC.AddRange(alternativaCRespondida);
        alternativaD.AddRange(alternativaDRespondida);
        corretas.AddRange(corretaRespondida);
    }

    public void ChangeScene()
    {
        if (isConnected && Advertisement.IsReady("rewardedVideo"))
        {
            if (!justOnce)
            {
                Time.timeScale = 0f;
                ModalContinue.SetActive(true);
                justOnce = true;
            }
            else {
                Application.LoadLevel("notaFinal");
                Time.timeScale = 1.0f;
            }
        }
        else
            Application.LoadLevel("notaFinal");
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult obj)
    {
        switch (obj)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                ModalContinue.SetActive(false);
                RequestQuestion();
                Time.timeScale = 1.0f;
                GreenBar.fillAmount = 1;
                RequestQuestion();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    IEnumerator checkInternetConnection(Action<bool> action)
    {
        WWW www = new WWW("http://google.com");
        yield return www;
        if (www.error != null)
        {
            action(false);
        }
        else {
            action(true);
        }
    }

    void OnDisable()
    {
        PlayerPrefs.SetInt("acertos", acertos);
    }

    void InitializeLists()
    {
        perguntas = new List<string>();
        var index = 1;
        while(LEManager.IsThereKey("question" + index))
        {
            perguntas.Add(LEManager.GetLocString("question" + index++));
        };

        alternativaA = new List<string>();
        index = 1;
        while (LEManager.IsThereKey("alternativaA" + index))
        {
            alternativaA.Add(LEManager.GetLocString("alternativaA" + index++));
        };

        alternativaB = new List<string>();
        index = 1;
        while (LEManager.IsThereKey("alternativaB" + index))
        {
            alternativaB.Add(LEManager.GetLocString("alternativaB" + index++));
        };

        alternativaC = new List<string>();
        index = 1;
        while (LEManager.IsThereKey("alternativaC" + index))
        {
            alternativaC.Add(LEManager.GetLocString("alternativaC" + index++));
        };

        alternativaD = new List<string>();
        index = 1;
        while (LEManager.IsThereKey("alternativaD" + index))
        {
            alternativaD.Add(LEManager.GetLocString("alternativaD" + index++));
        };

        corretas = new List<string>();
        index = 1;
        while (LEManager.IsThereKey("corretas" + index))
        {
            corretas.Add(LEManager.GetLocString("corretas" + index++));
        };
    }
}
