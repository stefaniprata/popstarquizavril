﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdMobManager : MonoBehaviour {
    InterstitialAd interstitial;
    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = Constants.AdUnitId;
#elif UNITY_IPHONE
        string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    public void ShowInterstitial()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
    }

    public bool IsInterstitialLoaded()
    {
        return interstitial.IsLoaded();
    }

    void Start()
    {
        RequestInterstitial();
        Debug.Log("To no Start");

        DontDestroyOnLoad(this);
    }


}
