﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeEffect : MonoBehaviour {
	public float Speed;
	Image _image;

	void Start () {
		_image = GetComponent<Image> ();
        _image.canvasRenderer.SetAlpha(0.0f);

        FadeIn();
	}

	void FadeOut()
    {
        _image.CrossFadeAlpha(0, Speed, false);
        Invoke("FadeIn", Speed);
    }

    void FadeIn()
    {
        _image.CrossFadeAlpha(1.0f, Speed, false);
        Invoke("FadeOut", Speed);
    }
}
