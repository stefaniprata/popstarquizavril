﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour {

	void Start () {
        Invoke("ChangeScene", 2);
	}

    void ChangeScene()
    {
        SceneManager.LoadScene(Constants.GameScene);
    }
}
