﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Constants {

    public static List<string> FacebookPermissions = new List<string>() { "public_profile", "email", "user_friends" };
    public static string LeaderboardId = "CgkI-5y41_IVEAIQAA";
    public static string TituloScene = "titulo";
	public static string LoadingScene = "Loading";
	public static string GameScene = "T1";
    public static string AdUnitId = "ca-app-pub-2145505648245534/5647078409";
    public static string EN = "en-US";
    public static string PT = "pt-BR";
}
