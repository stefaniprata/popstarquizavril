﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour
{
    private bool _isEnable;

    #region Singleton
    private static AudioController _instance;

    public static AudioController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<AudioController>();

                if (_instance == null)
                {
                    GameObject services = new GameObject("__AudioController");
                    _instance = services.AddComponent<AudioController>();
                }
            }
            return _instance;
        }
    }
    #endregion

    void Awake()
    {
        InitializeSound();

        DontDestroyOnLoad(this);
    }

    private void InitializeSound()
    {
        if (!PlayerPrefs.HasKey("SoundEnable"))
        {
            PlayerPrefs.SetInt("SoundEnable", 1);

            EnableAllSound();
        }
        else
        {
            _isEnable = PlayerPrefs.GetInt("SoundEnable") == 1 ? true : false;
        }
    }

    public void MuteAllSounds()
    {
        AudioSource audio = GameObject.FindObjectOfType<AudioSource>();
        audio.mute = true;
        PlayerPrefs.SetInt("SoundEnable", 0);
    }

    public void EnableAllSound()
    {
        AudioSource audio = GameObject.FindObjectOfType<AudioSource>();
        audio.mute = false;

        audio.Play();

        PlayerPrefs.SetInt("SoundEnable", 1);
    }

    public bool IsEnable()
    {
        return PlayerPrefs.GetInt("SoundEnable") == 1 ? true : false;
    }

    public void PlaySoundEffect(AudioSource source, AudioClip clip)
    {
        source.PlayOneShot(clip);
    }
}