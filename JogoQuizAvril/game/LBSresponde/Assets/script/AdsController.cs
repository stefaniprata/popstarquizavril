﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api;

public class AdsController : MonoBehaviour {

    private ChartbostController m_chartboost;
    private AdMobManager m_adMob;

    void Start () {
        m_chartboost = GameObject.FindObjectOfType<ChartbostController>();
        m_adMob = GameObject.FindGameObjectWithTag("AdMob").GetComponent<AdMobManager>();
        Debug.Log(m_adMob == null);

        VerifyAds();
    }

    private void VerifyAds()
    {
        if (!PlayerPrefs.HasKey("numberads"))
            PlayerPrefs.SetInt("numberads", 0);
        else
        {
            var numberAds = PlayerPrefs.GetInt("numberads");
            if (numberAds < 2) PlayerPrefs.SetInt("numberads", numberAds + 1);
            else if (numberAds == 2)
            {
                PlayerPrefs.SetInt("numberads", 0);
                CallAds();
            }
        }
    }

    private void CallAds()
    {
        if (!PlayerPrefs.HasKey("Ads"))
        {
            PlayerPrefs.SetString("Ads", "Video");
            ShowVideo();
        }

        else
        {
            string adsType = PlayerPrefs.GetString("Ads");
            if (adsType.Equals("Video"))
            {
                if (m_adMob.IsInterstitialLoaded())
                {
                    m_adMob.ShowInterstitial();
                    PlayerPrefs.SetString("Ads", "Interstiitial");
                }
                else if (m_chartboost.IsInterstitialCached())
                {
                    m_chartboost.ShowInterstitial();
                    PlayerPrefs.SetString("Ads", "Interstiitial");
                }
                else {
                    ShowVideo();
                    PlayerPrefs.SetString("Ads", "Video");
                }

            }
            else if (adsType.Equals("Interstiitial"))
            {
                if (Advertisement.IsReady())
                {
                    PlayerPrefs.SetString("Ads", "Video");
                    ShowVideo();
                }
                else {
                    if (m_adMob.IsInterstitialLoaded())
                    {
                        m_adMob.ShowInterstitial();
                        PlayerPrefs.SetString("Ads", "Interstiitial");
                    }
                    else {
                        m_chartboost.ShowInterstitial();
                        PlayerPrefs.SetString("Ads", "Interstiitial");
                    }
                }
            }
        }
    }

    private void ShowVideo()
    {
        if (Advertisement.IsReady())
        {
            Advertisement.Show();
        }
    }
}
