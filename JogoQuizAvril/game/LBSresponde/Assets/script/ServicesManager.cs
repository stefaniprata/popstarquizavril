﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;

public class ServicesManager : MonoBehaviour
{

    #region Singleton
    private static ServicesManager _instance;
    public static ServicesManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ServicesManager>();
            }
            return _instance;
        }
    }
    #endregion

    #region Unity Methods
    void Start () {
        DontDestroyOnLoad(this);

        PlayGamesPlatform.Activate();

        Social.localUser.Authenticate((bool sucess) =>
        {
            if (sucess) Debug.Log("Logou no google play");
            else Debug.Log("N Logou no google play");
        });
	}

    #endregion

    #region Google methods
    /// <summary>
    /// A progress of 0.0f means revealing the achievement and a progress of 100.0f means unlocking the achievement
    /// </summary>
    /// <param name="achievementId"></param>
    /// <param name="value"></param>
    /// <param name="callback"></param>
    public void UnlockAchievement(string achievementId, float value, Action<bool> callback)
    {
        Social.ReportProgress(achievementId, value, callback);
    }

    /// <summary>
    /// Use only if your achievement is incremental
    /// </summary>
    /// <param name="achievementId"></param>
    /// <param name="value"></param>
    /// <param name="callback"></param>
    public void IncrementAchievement(string achievementId, int value, Action<bool> callback)
    {
        PlayGamesPlatform.Instance.IncrementAchievement(achievementId, value, callback);
    }

    public void PostScore(int score, string leaderboardId, Action<bool> callback)
    {
        Social.ReportScore(score, leaderboardId, callback);
    }

    /// <summary>
    /// Show all your achievements
    /// </summary>
    public void ShowAchievements()
    {
        Social.ShowAchievementsUI();
    }

    /// <summary>
    /// Show all Leaderboards
    /// </summary>
    public void ShowLeaderboard()
    {
        if (Social.localUser.authenticated)
            Social.ShowLeaderboardUI();
        else
            Social.localUser.Authenticate((bool sucess) =>
            {
                if (sucess)
                    Social.ShowLeaderboardUI();
            });
    }

    /// <summary>
    /// It shows a particular leaderboard that you wish
    /// </summary>
    /// <param name="leaderboardId"></param>
    public void ShowLeaderboard(string leaderboardId)
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboardId);
    }

    /// <summary>
    /// It shows a particular leaderboard that you wish and execute an action after that
    /// </summary>
    /// <param name="leaderboardId"></param>
    /// <param name="status"></param>
    public void ShowLeaderboard(string leaderboardId, Action<UIStatus> status)
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI(leaderboardId, status);
    }

    /// <summary>
    /// After sign out is necessary call the authentication method again
    /// </summary>
    public void Signout()
    {
        PlayGamesPlatform.Instance.SignOut();
    }
    #endregion

}
